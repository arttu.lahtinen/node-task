# Node Task

Hello readme

## Installation steps

1. Clone repository `git clone`
2. Install dependencies
3. Start the app

## Terminal commands

```sh
git clone https://gitlab.com/arttu.lahtinen/node-task
cd ./node-task
npm i
```
Start cmd: `npm run start`



## TO DO

- [x] Git ignore
- [ ] Software test
- [ ] Code comments
